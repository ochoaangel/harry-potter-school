import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Character } from '../interfaces/interface';


@Injectable({
  providedIn: 'root'
})
export class UtilityService {



  constructor() { }


  cleanCharacters(data) {

    data = data.map((oneCharacter: Character) => {

      // getting the age
      let age = 0;
      if (oneCharacter.dateOfBirth) {
        const userDOB = moment(oneCharacter.dateOfBirth, 'DD-MM-YYYY');
        age = moment().diff(userDOB, 'years')
      } else if (oneCharacter.yearOfBirth) {
        age = moment().year() - oneCharacter.yearOfBirth
      }

      return { name: oneCharacter.name, age, patronus: oneCharacter.patronus, image: oneCharacter.image, }

    })

    return data;
  }

  sortByString(list, key) {
    return list.sort((a, b) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0))
  }

  sortByNumber(list, key) {
    return list.sort((a, b) => {
      if (a[key] > b[key]) {
        return 1;
      } else if (a[key] < b[key]) {
        return -1;
      } else {
        return 0;
      }
    })

  }


  search(list, text) {

    list = list.filter(x => {
      let txt = text.toString().toLowerCase();
      let n = x.name.toString().toLowerCase();
      let a = x.age.toString().toLowerCase();
      let p = x.patronus.toString().toLowerCase();
      return n.includes(text) || a.includes(text) || p.includes(text)
    })

    return list
  }

}
