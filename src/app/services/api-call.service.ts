import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Character } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'https://hp-api.herokuapp.com/api/characters';
  }

  getCharacters(personaje: string): Observable<any> {
    const url = `/house/${personaje}`;
    return this.http.get<Character>(this.baseUrl + url);
  }

  getStudents(): Observable<any> {
    const url = `/students`;
    return this.http.get<Character>(this.baseUrl + url);
  }

  getTeachers(): Observable<any> {
    const url = `/staff`;
    return this.http.get<Character>(this.baseUrl + url);
  }

}
