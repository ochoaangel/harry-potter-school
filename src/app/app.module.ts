import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

// myModules
import { MaterialModule } from './material/material.module';

// myComponents
import { ContentComponent } from './components/others/content/content.component';
import { TeachersComponent } from './components/main-elements/teachers/teachers.component';
import { StudentsComponent } from './components/main-elements/students/students.component';
import { CharactersComponent } from './components/main-elements/characters/characters.component';
import { OneCharacterComponent } from './components/others/one-character/one-character.component';
import { HeaderSelectorComponent } from './components/others/header-selector/header-selector.component';
import { SubheaderSelectorComponent } from './components/others/subheader-selector/subheader-selector.component';
import { SortAndSearchComponent } from './components/others/sort-and-search/sort-and-search.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    TeachersComponent,
    StudentsComponent,
    CharactersComponent,
    HeaderSelectorComponent,
    SubheaderSelectorComponent,
    OneCharacterComponent,
    SortAndSearchComponent,
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
