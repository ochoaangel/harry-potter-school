import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header-selector',
  templateUrl: './header-selector.component.html',
  styleUrls: ['./header-selector.component.scss']
})
export class HeaderSelectorComponent implements OnInit {

  @Input() title;
  @Output() public visible: EventEmitter<any> = new EventEmitter();

  btnVisible = false;

  constructor() { }

  ngOnInit(): void {
  }

  btn() {
    this.btnVisible = !this.btnVisible;
    this.visible.emit(this.btnVisible)
  }


}
