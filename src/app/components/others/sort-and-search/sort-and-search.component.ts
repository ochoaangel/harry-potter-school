import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-sort-and-search',
  templateUrl: './sort-and-search.component.html',
  styleUrls: ['./sort-and-search.component.scss']
})
export class SortAndSearchComponent implements OnInit {

  edit;
  @ViewChild('todo') resetSearch: ElementRef;
  @Output() public textoSortSearch: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  sortBy(parameter) {
    this.edit = parameter;
    this.textoSortSearch.emit(parameter)
  }

  onEnter(e) {
    this.textoSortSearch.emit(e)
  }

  btnSearch(e) {
    this.textoSortSearch.emit(e)
  }

  btnCancel() {
    this.textoSortSearch.emit('');
    this.resetSearch.nativeElement.value = '';
  }

}
