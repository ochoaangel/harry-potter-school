import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SortAndSearchComponent } from './sort-and-search.component';

describe('SortAndSearchComponent', () => {
  let component: SortAndSearchComponent;
  let fixture: ComponentFixture<SortAndSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SortAndSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SortAndSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
