import { Component, Input, OnInit, Output } from '@angular/core';
import { ApiCallService } from 'src/app/services/api-call.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  // export class ContentComponent implements OnInit {

  @Input() charactersList
  @Input() set textoSortSearchToContent(value: string) { this.sortOrSearch(value) }

  myItem;
  house;
  houses = [
    { name: 'slytherin', active: true },
    { name: 'gryffindor', active: false },
    { name: 'ravenclaw', active: false },
    { name: 'hufflepuff', active: false }
  ];
  myResults;

  constructor(private apiCall: ApiCallService, private utility: UtilityService) { }

  ngOnInit(): void {

    // Defining house 0 by default

    this.house = this.house || this.houses[0].name;

    this.myItem = this.charactersList;

    if (localStorage.getItem(this.myItem)) {
      // OtherTimes
      switch (this.myItem.toLowerCase()) {
        case 'students':
          this.myResults = JSON.parse(localStorage.getItem(this.myItem))
          break;

        case 'teachers':
          this.myResults = JSON.parse(localStorage.getItem(this.myItem))
          break;

        case 'characters':
        default:
          this.myResults = JSON.parse(localStorage.getItem(this.myItem))
          break;
      }



    } else {
      // firstTime

      switch (this.myItem.toLowerCase()) {
        case 'students': this.apiCall.getStudents().subscribe(res => this.handleRes(res)); break;
        case 'teachers': this.apiCall.getTeachers().subscribe(res => this.handleRes(res)); break;
        case 'characters':
        default:
          this.apiCall.getCharacters(this.house).subscribe(res => this.handleRes(res))
          break;
      }
    }



  }

  handleRes(res) {
    this.myResults = this.utility.cleanCharacters(res)
    localStorage.setItem(this.myItem, JSON.stringify(this.myResults))
  }

  selectedHouse(house) {
    this.apiCall.getCharacters(house).subscribe(res => this.handleRes(res))
  }

  sortOrSearch(value) {
    switch (value) {
      case undefined: break;
      case '':
        this.myResults = JSON.parse(localStorage.getItem(this.myItem));
        break;
      case 'age': this.utility.sortByNumber(this.myResults, value); break;
      case 'name':
      case 'patronus': this.utility.sortByString(this.myResults, value); break;
      default:
        this.myResults = this.utility.search(JSON.parse(localStorage.getItem(this.myItem)), value);
        break;
    }
  }


}

