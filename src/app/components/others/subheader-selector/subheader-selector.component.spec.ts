import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubheaderSelectorComponent } from './subheader-selector.component';

describe('SubheaderSelectorComponent', () => {
  let component: SubheaderSelectorComponent;
  let fixture: ComponentFixture<SubheaderSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubheaderSelectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubheaderSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
