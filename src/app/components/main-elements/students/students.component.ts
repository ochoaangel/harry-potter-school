import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  edit;
  show = false
  myItem = 'Students'
  @Input() textoSortSearchToContent;

  constructor() { }

  ngOnInit(): void { }

  visible(e) {
    this.show = e;
  }

  textoSortSearch(e) {
    this.textoSortSearchToContent = e
  }

}
