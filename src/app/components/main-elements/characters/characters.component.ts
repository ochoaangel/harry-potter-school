import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  edit;
  show = false
  myItem = 'Characters'
  @Input() textoSortSearchToContent;

  constructor() { }

  ngOnInit(): void { }

  visible(e) {
    this.show = e;
  }

  textoSortSearch(e) {
    this.textoSortSearchToContent = e
  }

}


