import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {

  edit;
  show = false
  myItem = 'Teachers'
  @Input() textoSortSearchToContent;


  constructor() { }

  ngOnInit(): void { }

  visible(e) {
    this.show = e;
  }

  textoSortSearch(e) {
    this.textoSortSearchToContent = e
  }

}
